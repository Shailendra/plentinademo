//
//  PlentinaApp.swift
//  Plentina
//
//  Created by Shailendra on 28/11/21.
//

import SwiftUI

@main
struct PlentinaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
