//
//  Constant.swift
//  Plentina
//
//  Created by Shailendra on 26/11/21.
//
import Foundation

//MARK:- API Keys

// Basic
let kContentTypeKey        = "Content-Type"
let kContentTypeValueKey   = "application/json"
let kContentLengthKey      = "Content-Length"

let kQuotes                = "quotes"




//===========================================
// MARK:- HTTP METHODS
//===========================================
var kPOST    = "POST"
var kPUT     = "PUT"
var kGET     = "GET"
var kDELETE  = "DELETE"



//===========================================
// MARK:- BASE URL
//===========================================

var kBaseURL = "https://animechan.vercel.app/api/quotes"

