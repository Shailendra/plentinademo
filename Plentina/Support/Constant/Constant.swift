//
//  Constant.swift
//  Plentina
//
//  Created by Shailendra on 26/11/21.
//

import Foundation



let kTokenAPI            = "TokenAPI"


//=======================
// Message
//=======================

let kInternetTitle        = "No Internet Connection"
let kInternetDiscription  = "Make sure your device is connected to the internet!"

let kSomeThingWrong       = "Oops something went wrong please try again."



let kUpgradeNow  = "Upgrade Now"
let kClose       = "Close"
let kAlert       = "Alert"
let kOK          = "OK"
let kYes         = "yes"


// ***************************************
// MARK:- Set Token API
// ***************************************
func setTokenAPICall(val: String){
    UserDefaults.standard.set(val, forKey: kTokenAPI)
    UserDefaults.standard.synchronize()
}

// ***************************************
// MARK:- Get Token API
// ***************************************
func getTokenAPICall()-> String{
    if UserDefaults.standard.value(forKey: kTokenAPI) != nil{
        return UserDefaults.standard.value(forKey: kTokenAPI) as! String
    }
    else{
        return ""
    }
}

