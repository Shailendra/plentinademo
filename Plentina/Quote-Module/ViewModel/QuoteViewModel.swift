//
//  HomeViewModel.swift
//  Plentina
//
//  Created by Shailendra on 27/11/21.
//

import Foundation
import Combine

class QuoteVM: ObservableObject, Identifiable{

    private var task: AnyCancellable?
    
    @Published var quotes =  [Quote]()
    
    func fetchQuotes() {
        task = URLSession.shared.dataTaskPublisher(for: URL(string: kBaseURL)!)
            .map { $0.data }
            .decode(type: [Quote].self, decoder: JSONDecoder())
            .replaceError(with: [])
            .eraseToAnyPublisher()
            .receive(on: RunLoop.main)
            .assign(to: \QuoteVM.quotes, on: self)
    }
}
