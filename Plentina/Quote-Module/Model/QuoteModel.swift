//
//  HomeModel.swift
//  Plentina
//
//  Created by Shailendra on 27/11/21.
//

import Foundation


struct Quote : Codable {
    var id        = UUID()
    let anime     : String
    let character : String
    let quote     : String
    
    enum CodingKeys: String, CodingKey {

        case anime     = "anime"
        case character = "character"
        case quote     = "quote"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        anime = try values.decodeIfPresent(String.self, forKey: .anime) ?? .kEmpty
        character = try values.decodeIfPresent(String.self, forKey: .character)  ?? .kEmpty
        quote = try values.decodeIfPresent(String.self, forKey: .quote) ?? .kEmpty
    }
    
}
