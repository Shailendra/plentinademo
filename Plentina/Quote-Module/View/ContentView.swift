//
//  ContentView.swift
//  Plentina
//
//  Created by Shailendra on 28/11/21.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var viewModel = QuoteVM()
    
    var body: some View {
        NavigationView {
            List(viewModel.quotes, id: \.id) { quote in
                QuotesView(quote: quote)
            }.navigationBarTitle("Quotes")
                .onAppear {
                    self.viewModel.fetchQuotes()
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}



