//
//  QuotesView.swift
//  Plentina
//
//  Created by Shailendra on 28/11/21.
//

import SwiftUI

struct QuotesView: View {
    
    private let quote: Quote
    
    init(quote: Quote) {
        self.quote = quote
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text(quote.anime)
                .font(.title3)
                .foregroundColor(Color.blue)
            Text("\(quote.quote)")
                .font(.system(size: 13))
                .foregroundColor(Color.gray)
            Text("\(quote.character)")
                .font(.system(size: 13))
                .foregroundColor(Color.gray)
        }
    }
}
